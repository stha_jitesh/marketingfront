# [Start Bootstrap] - [Shop Homepage]

[Shop Homepage] is a basic HTML online store homepage template for [Bootstrap] created by [Start Bootstrap].

## Getting Started

To begin using this template, choose one of the following options to get started:
* [Download the latest release on Start Bootstrap]

## Bugs and Issues

Have a bug or an issue with this template? Try to fix into here on GitHub or leave a comment on the on our group chat.

## Creator Of Jitesh

This template is downloaded from bootstrap theme so we can change with our desire.
Owner : Jitesh
Admin : Abhishek, Satish, Ukesh 


Start Bootstrap is based on the [Bootstrap] framework created by [Mark Otto] and [Jacob Thorton].

## Copyright and License

Copyright 2013-2016 Blackrock Digital LLC. Code released under the MIT license.